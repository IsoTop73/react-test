import { combineReducers } from 'redux';
import selectedWidgetId from './selectWidget';
import widgets from './widgets';

const rootReducer = combineReducers({selectedWidgetId, widgets});

export default rootReducer;