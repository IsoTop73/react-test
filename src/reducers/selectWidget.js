import { SELECT_WIDGET } from '../actions/actionsTypes';

export default (state = null, action) => {
  switch (action.type) {
    case SELECT_WIDGET:
      return action.id;
    default:
      return state;
  }
}