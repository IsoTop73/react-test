import { SAVE_WIDGET, ADD_WIDGET } from '../actions/actionsTypes';
import { store } from '../index';

export default (state = [], action) => {
  switch (action.type) {
    case SAVE_WIDGET:
      let id = store.getState().selectedWidgetId;
      let newState = [...state];
      newState[id] = action.widget;
      return newState;
    case ADD_WIDGET:
      return [...state, action.widget];
    default:
      return state;
  }
}