import { SELECT_WIDGET } from './actionsTypes';

export default (id) => {
  return {
    type: SELECT_WIDGET,
    id
  }
}