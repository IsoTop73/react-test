import { SAVE_WIDGET } from './actionsTypes';

export default (widget) => {
  return {
    type: SAVE_WIDGET,
    widget
  }
}