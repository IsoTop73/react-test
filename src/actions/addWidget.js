import { ADD_WIDGET } from './actionsTypes';

export default (widget) => {
  return {
    type: ADD_WIDGET,
    widget
  }
}