export default (color) => {
  if (color.indexOf(',') === -1)
    return false;

  let rgb = color.split(',');

  if (rgb.length !== 3)
    return false;

  return rgb.every((elem) => {
    let color = parseInt(elem);

    if (isNaN(color))
      return false;

    return (color <= 255 && color >= 0)
  });
}