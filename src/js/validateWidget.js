import validateColor from './validateColor';

export default (widget) => {
  let { name, count, color } = widget;

  if (!name) {
    alert('Name is required');
    return false;
  }

  if (!count) {
    alert('Count is required');
    return false;
  }

  if (color) {
    if (!validateColor(color)) {
      alert('Invalid color value!\nYou can enter a color value in RGB.\nExample: 255, 255, 255');
      return false;
    }
  }

  return true;
}