import React from 'react';
import Settings from './Settings';
import Dashboard from './Dashboard';
import '../styles/App.styl';
import 'normalize.css/normalize.css';

class App extends React.Component {
  render() {
    return (
      <div id="widget-app">
        <Settings/>
        <Dashboard/>
      </div>
    );
  }
}

export default App;