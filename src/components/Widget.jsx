import React from 'react';
import '../styles/Widget.styl';
import selectWidget from '../actions/selectWidget';
import { connect } from 'react-redux'


class Widget extends React.Component {
  constructor(props) {
    super(props);
    this.onWidgetClick = this.onWidgetClick.bind(this);
  }

  onWidgetClick() {
    let id = this.props.id;

    this.props.onWidgetClick(id);
  }

  render() {
    return (
      <div className="widget" onClick={this.onWidgetClick}
        style={{
          backgroundColor: `rgb(${this.props.color})`
        }}>
        <span className="widget-name"> Name: {this.props.name}</span>
        <span className="widget-count">Count: {this.props.count}</span>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {}
};

const mapDispatchToProps = (dispatch) => {
  return {
    onWidgetClick: (id) => {
      dispatch(selectWidget(id));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Widget);