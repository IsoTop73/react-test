import React from 'react';
import { connect } from 'react-redux';
import { store } from '../index';
import '../styles/Settings.styl';
import saveWidget from '../actions/saveWidget';
import addWidget from '../actions/addWidget';
import selectWidget from '../actions/selectWidget';
import validateWidget from '../js/validateWidget';

class Settings extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      nameValue: '',
      countValue: '',
      colorValue: '',
      saveDisabled: true
    };

    store.subscribe(() => {
      let state = store.getState();
      let id = state.selectedWidgetId;
      let widget = state.widgets[id];
      let cssColor = id !== null
      ? getComputedStyle(document.getElementsByClassName('widget')[id])
      ['background-color'].slice(4, -1)
      : '';

      this.setState({
        nameValue: widget ? widget.name : '',
        countValue: widget ? widget.count : '',
        colorValue: widget
          ? 'color' in widget && widget.color !== '' ? widget.color : cssColor
          : '',
        saveDisabled: !widget
      })
    });

    this.handleInputName = this.handleInputName.bind(this);
    this.handleInputCount = this.handleInputCount.bind(this);
    this.handleInputColor = this.handleInputColor.bind(this);
    this.saveWidgetHandle = this.saveWidgetHandle.bind(this);
    this.addWidgetHandle = this.addWidgetHandle.bind(this);
    this.getWidget = this.getWidget.bind(this);
  }

  handleInputName(e) {
    this.setState({
      nameValue: e.target.value
    });
  }

  handleInputCount(e) {
    this.setState({
      countValue: e.target.value
    });
  }

  handleInputColor(e) {
    this.setState({
      colorValue: e.target.value
    });
  }

  getWidget() {
    return {
      name: this.state.nameValue,
      count: this.state.countValue,
      color: this.state.colorValue
    }
  }

  saveWidgetHandle() {
    let widget = this.getWidget();

    if (!validateWidget(widget))
      return;

    this.props.saveWidget(widget);
    this.props.deselectWidget();
  }
  
  addWidgetHandle() {
    let widget = this.getWidget();

    if (!validateWidget(widget))
      return;

    this.props.addWidget(widget);
    this.props.deselectWidget();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.nameValue !== this.state.nameValue
      || nextState.countValue !== this.state.countValue
      || nextState.colorValue !== this.state.colorValue) {
      return true;
    }
    return false;
  }

  render() {
    return (
      <div id="settings">
        <h2 className="title">Settings</h2>
        <input type="text"
          placeholder="Name"
          onChange={this.handleInputName}
          value={this.state.nameValue}/>
        <input type="number"
          placeholder="Count"
          onChange={this.handleInputCount}
          value={this.state.countValue}/>
        <input type="text"
          placeholder="Color: RGB"
          onChange={this.handleInputColor}
          value={this.state.colorValue}/>
        <button className="btn btn-save"
          disabled={this.state.saveDisabled}
          onClick={this.saveWidgetHandle}>
          Save
        </button>
        <button className="btn btn-add"
          onClick={this.addWidgetHandle}>
          Add widget
        </button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveWidget: (widget) => {
      dispatch(saveWidget(widget));
    },
    addWidget: (widget) => {
      dispatch(addWidget(widget));
    },
    deselectWidget: () => {
      dispatch(selectWidget(null));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);