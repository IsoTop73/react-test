import React from 'react';
import Widget from './Widget';
import { connect } from 'react-redux';
import { store } from '../index';
import '../styles/Dashboard.styl';


class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      widgets: this.props.widgets
    };

    store.subscribe(() => {
      this.setState({
        widgets: store.getState().widgets
      })
    });
  }

  render() {
    return (
      <div id="dashboard">
        <h2 className="title">Dashboard</h2>
        {
          this.state.widgets.map((el, index) => {
            return (
              <Widget name={el.name}
                count={el.count}
                color={el.color}
                key={index}
                id={index}
              />
            )
          })
        }
      </div>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    widgets: state.widgets
  }
}

export default connect(mapStateToProps)(Dashboard);